﻿using System;
using System.Net;
using System.Web;
using NoiHub.Models;
using NoiHub.Services;
using Plugin.Settings;
using Plugin.Settings.Abstractions;
using Xamarin.Forms;

namespace NoiHub.Utility
{
    public class Sistema
    {

        public static string URL_DOMINIO = "https://m.noihub.it/";

        public Sistema()
        {
        }

        private static ISettings AppSettings
        {
            get
            {
                return CrossSettings.Current;
            }
        }



        public static string UserName
        {
            get => AppSettings.GetValueOrDefault(nameof(UserName), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(UserName), value);
        }

        public static string TokenUtente
        {
            get => AppSettings.GetValueOrDefault(nameof(TokenUtente), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(TokenUtente), value);
        }


        public static string DatiOffline
        {
            get => AppSettings.GetValueOrDefault(nameof(DatiOffline), string.Empty);
            set => AppSettings.AddOrUpdateValue(nameof(DatiOffline), value);
        }


        public static void Logout()
        {
            //UserName = string.Empty;
            TokenUtente = string.Empty;
            DatiOffline = string.Empty;
            //myView.NomeUtente = string.Empty;
        }


        /*
        public static void VerificaLogin(MyWebView web_view, string url, MyView view_menu)
        {
            if (url.Contains(URL_DOMINIO + "api/") && !url.Contains("action=checkToken"))
            {
                //string risposta = await web_view.EvaluateJavaScriptAsync("document.body.innerHTML");
                WebClient wc = new WebClient();
                wc.Headers.Add("user-agent", Device.RuntimePlatform.ToLower());
                var risposta = wc.DownloadString(url);
                Response r = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(risposta.Replace("\\", ""));

                if (r.status == "200")
                {
                    //LOGIN OK

                    Sistema.TokenUtente = r.token;
                    Sistema.UserName = r.nome + " " + r.cognome;
                    //Setto il nome dell'utente nel menu
                    //view_menu.NomeUtente = Sistema.UserName;

                }
                else
                {
                    //LOGIN KO

                    Sistema.Logout();
                }


                web_view.Source = URL_DOMINIO + r.go_to;

            }
        }
        */


        /*   
        public static void CheckToken(MyWebView web_view, string url, MyView view_menu, string url_destinazione)
        {
            if (url.Contains("action=checkToken"))
            {
                //string risposta = await web_view.EvaluateJavaScriptAsync("document.body.innerHTML");
                WebClient wc = new WebClient();
                wc.Headers.Add("user-agent", Device.RuntimePlatform.ToLower());
                var risposta = wc.DownloadString(url);

                Response r = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(risposta.Replace("\\", ""));

                if (r.status != "200")
                {
                    //LOGIN KO
                    Sistema.Logout();
                }


                web_view.Source = URL_DOMINIO + url_destinazione + "?token=" + r.token + "&device=" + Device.RuntimePlatform.ToLower() + "&versione=" + DependencyService.Get<IAppVersion>().GetBuild();
            }
        }
        */



        public static void CheckToken()
        {
            string urlCheckToken = "https://m.noihub.it/api/?action=checkToken&token="+TokenUtente;

            WebClient wc = new WebClient();
            wc.Headers.Add("user-agent", Device.RuntimePlatform.ToLower());
            var risposta = wc.DownloadString(urlCheckToken);

            Response r = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(risposta.Replace("\\", ""));

            if (r.status == "200")
            {
                //LOGIN OK
                DatiOffline = risposta;

            }
            else
            {
                Logout();
            }
        }



        public static Response GetResponse()
        {
            Response r = null;

            if (!string.IsNullOrWhiteSpace(DatiOffline))
                r = Newtonsoft.Json.JsonConvert.DeserializeObject<Response>(DatiOffline.Replace("\\", ""));

            return r;
        }



        public static bool AggiornamentoRichiesto(RootObject oggetto)
        {
            bool IsRichiesto = false;

            string localBuild = DependencyService.Get<IAppVersion>().GetBuild();

            var local = new Version(localBuild);
            var appStoreVersion = new Version(oggetto.current_version);

            var result = appStoreVersion.CompareTo(local);

            if (oggetto.force_upgrade && result > 0)
            {
                //Aggiornamento richiesto
                IsRichiesto = true;
            }

            return IsRichiesto;
        }


        public static string GetParametroFromUrl(string url)
        {
            Uri myUri = new Uri(url);
            string param1 = HttpUtility.ParseQueryString(myUri.Query).Get("token");

            return param1;
        }

    }
}
