﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Refit;
using NoiHub.Models;

namespace NoiHub
{
    public interface IRestApi
    {

        [Get("/config_json.php")]
        Task<RootObject> GetData();

    }
}

