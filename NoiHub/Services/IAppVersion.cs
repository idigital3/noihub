﻿using System;
namespace NoiHub.Services
{
    public interface IAppVersion
    {
        string GetVersion();
        string GetBuild();
    }
}
