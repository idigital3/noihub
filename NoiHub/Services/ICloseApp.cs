﻿using System;
namespace NoiHub.Services
{
    public interface ICloseApp
    {
        void CloseApplication();
    }
}
