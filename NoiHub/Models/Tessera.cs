﻿using System;
namespace NoiHub.Models
{
    public class Tessera
    {
        public string tessera { get; set; }
        public string nome { get; set; }
        public string cognome { get; set; }
        public string annoTessera { get; set; }
        public string barcode { get; set; }
        public string dataNascita { get; set; }
        public string regione { get; set; }
        public string diocesi { get; set; }
        public string circolo { get; set; }
    }
}
