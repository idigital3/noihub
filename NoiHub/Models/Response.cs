﻿using System;
using System.Collections.Generic;

namespace NoiHub.Models
{
    public class Response
    {
        public string status { get; set; }
        public string message { get; set; }
        public string token { get; set; }
        public string device { get; set; }
        public string tessera_attiva { get; set; }
        public List<Tessera> tessere_disponibili { get; set; }
        public bool isLogged { get; set; }
    }
}
