﻿using System;
using System.Collections.Generic;

namespace NoiHub.Models
{
    public class Profilo
    {
        public string logo { get; set; }
        public string welcome { get; set; }
        public List<Link> link { get; set; }
    }
}
