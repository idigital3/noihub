﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using NoiHub.Models;
using NoiHub.Services;
using NoiHub.Utility;
using Plugin.Connectivity;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;


namespace NoiHub
{
    public partial class MainPage : ContentPage
    {
        RootObject oggetto;
        string currentUrl;

        public MainPage()
        {
            InitializeComponent();

            Xamarin.Forms.NavigationPage.SetHasNavigationBar(this, false);
            On<Xamarin.Forms.PlatformConfiguration.iOS>().SetUseSafeArea(true);

            var tapGesture = new Xamarin.Forms.TapGestureRecognizer();
            tapGesture.Tapped += TapGesture_Tapped;

            img_home.GestureRecognizers.Add(tapGesture);
            img_menu.GestureRecognizers.Add(tapGesture);

            web_view.Navigated += Web_View_Navigated;
            web_view.Navigating += Web_View_Navigating;


            btnProfilo.Clicked += ButtonHomeClicked;
            btnTessera.Clicked += ButtonHomeClicked;
            btnStorico.Clicked += ButtonHomeClicked;
            btnCircoli.Clicked += ButtonHomeClicked;
        }





        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (!string.IsNullOrWhiteSpace(currentUrl) && currentUrl.Contains("profilo"))
                return;

            Init();
        }





        //Ha finito di navigare
        void Web_View_Navigated(object sender, WebNavigatedEventArgs e)
        {
            if (!CrossConnectivity.Current.IsConnected)
            {
                loading.IsVisible = false;
                return;
            }

            currentUrl = e.Url;

            //l_titolo.Text = ImpostaTitoloSezione(currentUrl);

            if (e.Url == Sistema.URL_DOMINIO + "?device=" + Device.RuntimePlatform.ToLower() + "&versione=" + DependencyService.Get<IAppVersion>().GetBuild())
            {
                loading.IsVisible = false;
                SetTitoloNavBar(oggetto.tapbar[0].navbar_title);
            }

            if(e.Url.Contains("token"))
            {
                string token = Sistema.GetParametroFromUrl(e.Url);
                Sistema.TokenUtente = token;
            }

            if(!string.IsNullOrWhiteSpace(Sistema.TokenUtente))
                Sistema.CheckToken();

            loading.IsVisible = false;

        }



        //Sta navigando
        void Web_View_Navigating(object sender, WebNavigatingEventArgs e)
        {
            if(e.Url.Contains("http"))
               loading.IsVisible = true;

            if (!CrossConnectivity.Current.IsConnected)
            {
                if(e.Url.Contains("tessera.php") && !string.IsNullOrWhiteSpace(Sistema.TokenUtente))
                {
                    //NESSUNA CONNESSIONE USO I DATI OFFLINE
                    PopolaHomePageOffline();
                    PopolaTesseraOffline();

                    web_view.IsVisible = false;
                    scrollHome.IsVisible = false;
                    scrollTessera.IsVisible = true;
                }
                else
                {
                    CreaAlert("Questa sezione è disponibile solamente online");

                    return;
                }
            }
        }



        void Init()
        {
            try
            {
                //Verifico se sono loggato
                if (!string.IsNullOrWhiteSpace(Sistema.TokenUtente))
                {
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        //NESSUNA CONNESSIONE USO I DATI OFFLINE
                        PopolaHomePageOffline();
                        PopolaTesseraOffline();

                        web_view.IsVisible = false;
                        scrollHome.IsVisible = true;
                        scrollTessera.IsVisible = false;
                    }
                    else
                    {
                        //CONNESSO e GESTIONE NORMALE
                        GestioneONLINE();
                    }
                }
                else
                {
                    //NON LOGGATO
                    GestioneONLINE();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                CreaAlert("Si è verificato un errore");
                return;
            }
        }




        public async void GestioneONLINE()
        {
            //Per far funzionare gli alert asincroni
            await Task.Delay(500);

            try
            {
                web_view.IsVisible = true;
                scrollHome.IsVisible = false;
                scrollTessera.IsVisible = false;

                if (!CrossConnectivity.Current.IsConnected)
                {
                    await DisplayAlert("Attenzione", "Connessione internet assente", "OK");
                    //Chiudo l'applicazione
                    DependencyService.Get<ICloseApp>().CloseApplication();

                    return;
                }


                oggetto = await RestApi.GetData();
                if (oggetto == null)
                {
                    CreaAlert("Si è verificato un errore");
                    return;
                }

                if (Sistema.AggiornamentoRichiesto(oggetto))
                {
                    CreaAlert("Si è verificato un errore");
                    return;
                }


                view_menu.Init(oggetto);
                view_menu.pageContent = this;
                loading.IsVisible = true;
                SetUrl(Sistema.URL_DOMINIO);

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                CreaAlert("Si è verificato un errore");
                return;
            }
        }




        public void SetTitoloNavBar(string titolo)
        {
            if (!string.IsNullOrWhiteSpace(titolo))
            {
                l_titolo.Text = titolo;
            }
        }


        public async void SetUrl(string url)
        {
            if (!string.IsNullOrWhiteSpace(url))
            {
                if (!CrossConnectivity.Current.IsConnected)
                {
                    await DisplayAlert("Attenzione", "Connessione internet assente", "OK");
                    //Chiudo l'applicazione
                    DependencyService.Get<ICloseApp>().CloseApplication();

                    return;
                }

                web_view.IsVisible = true;
                scrollHome.IsVisible = false;
                scrollTessera.IsVisible = false;

                loading.IsVisible = true;

                web_view.Source = url + "?device=" + Device.RuntimePlatform.ToLower() + "&versione=" + DependencyService.Get<IAppVersion>().GetBuild();

            }
        }


        void TapGesture_Tapped(object sender, EventArgs e)
        {
            string nome_componente = sender.GetType().Name;

            if (nome_componente.Equals("Image"))
            {
                Image image = (Image)sender;
                string class_name = image.ClassId;

                if (class_name.Equals("img_back"))
                {
                    if (web_view.CanGoBack && currentUrl != Sistema.URL_DOMINIO)
                    {
                        web_view.GoBack();
                    }
                }
                else if(class_name.Equals("img_menu"))
                {
                    if (!CrossConnectivity.Current.IsConnected)
                        CreaAlert("Le funzionalità del menu NON sono disponibili offline.");
                    else if(!Sistema.AggiornamentoRichiesto(oggetto))
                        view_menu.MenuVisible = !view_menu.MenuVisible;
                }
                else if(class_name.Equals("img_home"))
                {
                    if (!CrossConnectivity.Current.IsConnected)
                    {
                        //OFFLINE
                        PopolaHomePageOffline();
                        PopolaTesseraOffline();

                        web_view.IsVisible = false;
                        scrollHome.IsVisible = true;
                        scrollTessera.IsVisible = false;
                    }
                    else
                    {
                        //ONLINE
                        SetUrl(Sistema.URL_DOMINIO);
                    }
                }
            }

        }


        void ButtonHomeClicked(object sender, EventArgs e)
        {
            Button button = (Button)sender;

            if (button.ClassId == "btnTessera")
            {
                PopolaHomePageOffline();
                PopolaTesseraOffline();
                web_view.IsVisible = false;
                scrollHome.IsVisible = false;
                scrollTessera.IsVisible = true;
            }
            else
            {
                CreaAlert("Questa sezione è disponibile solamente online. Se sei online ti invitiamo a chiudere e riaprire l'applicazione. Grazie.");
            }

        }





        void PopolaHomePageOffline()
        {
            try
            {
                Response r = Sistema.GetResponse();
                Tessera tesseraAttiva = r.tessere_disponibili.FirstOrDefault(x => x.tessera == r.tessera_attiva);

                l_nome_home.Text = tesseraAttiva.nome.ToUpper() + " " + tesseraAttiva.cognome.ToUpper();
                l_numeroTessera_home.Text = tesseraAttiva.tessera.ToUpper();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CreaAlert("Si è verificato un errore, la invitiamo a riprovare più tardi. Grazie.");
            }

        }


        void PopolaTesseraOffline()
        {
            try
            {
                Response r = Sistema.GetResponse();
                Tessera tesseraAttiva = r.tessere_disponibili.FirstOrDefault(x => x.tessera == r.tessera_attiva);

                l_nome_tessera.Text = tesseraAttiva.nome.ToUpper() + " " + tesseraAttiva.cognome.ToUpper();
                l_annoTesseramento.Text = tesseraAttiva.annoTessera.ToUpper();

                img_barcode.Source = Xamarin.Forms.ImageSource.FromStream(
                () => new MemoryStream(Convert.FromBase64String(tesseraAttiva.barcode)));

                l_numeroTessera.Text = tesseraAttiva.tessera.ToUpper();
                l_dataNascita.Text = tesseraAttiva.dataNascita;
                l_regione.Text = tesseraAttiva.regione;
                l_diocesi.Text = tesseraAttiva.diocesi;
                l_circolo.Text = tesseraAttiva.circolo;

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                CreaAlert("Si è verificato un errore, la invitiamo a riprovare più tardi. Grazie.");
            }

        }



        public async void CreaAlert(string messaggio)
        {
            await DisplayAlert("Attenzione", messaggio, "OK");
        }





    }
}
