﻿using System;
using Android.Content.PM;
using NoiHub.Services;

[assembly: Xamarin.Forms.Dependency(typeof(NoiHub.Droid.Helpers.Version_Android))]
namespace NoiHub.Droid.Helpers
{
    public class Version_Android : IAppVersion
    {
        public Version_Android()
        {
        }

        public string GetVersion()
        {
            var context = global::Android.App.Application.Context;
            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionCode.ToString();
        }

        public string GetBuild()
        {
            var context = global::Android.App.Application.Context;

            PackageManager manager = context.PackageManager;
            PackageInfo info = manager.GetPackageInfo(context.PackageName, 0);

            return info.VersionName;
        }
    }
}
