﻿using System;
using NoiHub.Services;

[assembly: Xamarin.Forms.Dependency(typeof(NoiHub.Droid.Helpers.Close_Android))]
namespace NoiHub.Droid.Helpers
{
    public class Close_Android : ICloseApp
    {
        public Close_Android()
        {
        }

        public void CloseApplication()
        {

            Android.OS.Process.KillProcess(Android.OS.Process.MyPid());
            //var activity = (Activity)Forms.Context;
            //activity.FinishAffinity();
        }
    }
}
