﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Webkit;
using Android.Support.V4.Content;
using Android.Support.V4.App;

namespace NoiHub.Droid
{
    //[Activity(Label = "NoiHub", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    [Activity(Theme = "@style/MainTheme", Label = "NOI App", Icon = "@drawable/App_NOI_120", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        public static IValueCallback UploadMessage;
        private static int FILECHOOSER_RESULTCODE = 1;
        public static Android.Net.Uri mCapturedImageURI;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            //TabLayoutResource = Resource.Layout.Tabbar;
            //ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);
            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            if (ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.WriteExternalStorage) != Permission.Granted || ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.ReadExternalStorage) != Permission.Granted)
            {
                ActivityCompat.RequestPermissions(this, new String[] { Android.Manifest.Permission.WriteExternalStorage, Android.Manifest.Permission.ReadExternalStorage }, 2);
            }


            LoadApplication(new App());
        }


        protected override void OnActivityResult(int requestCode, Result resultCode, Android.Content.Intent data)
        {
            try
            {
                if (requestCode == FILECHOOSER_RESULTCODE)
                {
                    if (null == UploadMessage)
                        return;

                    Java.Lang.Object result = data == null ? mCapturedImageURI : data.Data;

                    UploadMessage.OnReceiveValue(new Android.Net.Uri[] { (Android.Net.Uri)result });
                    UploadMessage = null;
                }
                else if (requestCode == 2)
                {
                    //Permessi
                    base.OnActivityResult(requestCode, resultCode, data);
                }
                else
                    base.OnActivityResult(requestCode, resultCode, data);
            }
            catch (Exception ex)
            {
                base.OnActivityResult(requestCode, resultCode, data);
            }
        }


        public override void OnBackPressed()
        {
            Android.App.AlertDialog.Builder dialog = new AlertDialog.Builder(this);
            AlertDialog alert = dialog.Create();
            alert.SetTitle("Avviso");
            alert.SetMessage("Sei sicuro di volere uscire dall'applicazione?");
            alert.SetButton("OK", (c, ev) =>
            {
                // Ok button click task
                base.OnBackPressed();

            });
            alert.SetButton2("ANNULLA", (c, ev) => {


            });
            alert.Show();
        }
    }
}