﻿using System;
using System.Threading;
using NoiHub.Services;

[assembly: Xamarin.Forms.Dependency(typeof(NoiHub.iOS.Helpers.Close_IOS))]
namespace NoiHub.iOS.Helpers
{
    public class Close_IOS : ICloseApp
    {
        public Close_IOS()
        {
        }

        public void CloseApplication()
        {
            Thread.CurrentThread.Abort();
        }
    }
}
