﻿using System;
using Foundation;
using NoiHub.Services;

[assembly: Xamarin.Forms.Dependency(typeof(NoiHub.iOS.Helpers.Version_IOS))]
namespace NoiHub.iOS.Helpers
{
    public class Version_IOS : IAppVersion
    {
        public Version_IOS()
        {
        }

        public string GetBuild()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleVersion").ToString();
        }

        public string GetVersion()
        {
            return NSBundle.MainBundle.ObjectForInfoDictionary("CFBundleShortVersionString").ToString();
        }
    }
}
