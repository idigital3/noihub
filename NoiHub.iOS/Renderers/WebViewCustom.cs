﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Foundation;
using NoiHub;
using NoiHub.iOS.Renderers;
using NoiHub.Utility;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(MyWebView), typeof(WebViewCustom))]
namespace NoiHub.iOS.Renderers
{
    public class WebViewCustom : WebViewRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            var webView = this;
            webView.ScrollView.Bounces = false;

        }


        

    }
}
